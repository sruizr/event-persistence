import logging
import queue
import threading
import copy

from eventence.core.commands import (
    EntityAlreadyExist,
    EntityNotFound,
    EntityVersionAlreadyExist,
)
from eventence.ports import EventDataPort


logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)


class OnMemEventDataAdapter(EventDataPort):
    """On memory implementation of EvenDataPort."""

    def __init__(self):
        """Persist event dtos."""
        self._events = []
        self._entities = {}
        self._lock = threading.Lock()

    def add_event_dtos(self, dtos):
        """Add event_dtos to persistence system."""
        for dto in dtos:
            with self._lock:
                entity_uuid = dto.entity_uuid
                if dto.version == 0:
                    if entity_uuid in self._entities:
                        raise EntityAlreadyExist(entity_uuid)
                    self._entities[entity_uuid] = []
                if entity_uuid not in self._entities:
                    raise EntityNotFound(entity_uuid)
                if dto.version < len(self._entities[entity_uuid]):
                    raise EntityVersionAlreadyExist(entity_uuid, dto.version)

                index = len(self._events) + 1
                persisted_dto = dto.copy()
                persisted_dto.id = index
                self._entities[entity_uuid].append(index)
                self._events.append(persisted_dto)

    def get_entity_event_dtos(self, entity_uuid):
        """Return all event_dtos of a entity."""
        result = []
        if entity_uuid not in self._entities:
            raise EntityNotFound(entity_uuid)

        for index in self._entities[entity_uuid]:
            result.append(self._events[index - 1].copy())
        return result

    def collect_event_dtos(self, from_id, events_filter):
        """Return all event_dtos with id > from_id filtered."""
        result = []
        if len(self._events) <= from_id:
            return

        for dto in self._events[from_id + 1 :]:
            _, class_name = dto.entity_name.split(":")
            event_name = "{}.{}".format(class_name, dto.event_name)
            if event_name in events_filter:
                result.append(dto.copy())
        return result
