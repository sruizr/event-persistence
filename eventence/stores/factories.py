import importlib


class EventDataAdapterFactory:
    def __init__(self, engine=None):
        self._engine = engine if engine else "onmem"

        if self._engine == "onmem":
            module = importlib.import_module("eventence.stores.onmem")
            self._EventData = getattr(module, "OnMemEventDataAdapter")
        elif self._engine == "sqlite":
            module = importlib.import_module("eventence.stores.sqlite")
            self._EventData = getattr(module, "SqliteEventDataAdapter")
        elif self._engine == "postgresql":
            module = importlib.import_module("eventence.stores.postgresql")
            self._EventData = getattr(module, "PostgresqlEventDataAdapter")
        else:
            raise ValueError("Engine has not valid value")

    def create_event_data(self, *args, **kwargs):
        return self._EventData(*args, **kwargs)
