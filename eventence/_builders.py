"""Implement objects for testing purpose."""

import datetime
import uuid
import random
import copy
from eventence.ports import EventDataTransfer


class Observer:
    """Simple observer for testing purposes."""

    def __init__(self):
        """Observe events from noticers."""
        self._events = []

    def event_raised(self, event_dto):
        """Notify event."""
        self._events.append(event_dto)

    def get_last_event(self, position=1):
        """Return last event notified to observer."""
        return self._events[-position]


class Given:
    """Mother object for many builders."""

    def __init__(self):
        """Mother object of builders for testing purpose."""
        self._observer = Observer()

    @property
    def a_entity(self):
        """Entity object builder."""
        return EntityBuilder()

    @property
    def a_value(self):
        """Value object builder."""
        return ValueBuilder()

    # @property
    # def an_observer(self):
    # return SimpleBuilder(Observer)

    @property
    def a_event_dto(self):
        """Event dto builder."""
        return EventDtoBuilder()

    @property
    def a_entity_id(self):
        """Build an EventId object."""
        return EntityIdBuilder()


# class SimpleBuilder:
#     def __init__(self, Class):
#         self._class = Class

#     def build(self):
#         return self._class()


class Value:
    """Example of implementation of a value class."""

    def __init__(self, **kwargs):
        """Mock an example of value object."""
        self.__dict__.update(kwargs)

    def __setattr__(self, name, value):
        """Not allowed modifification of invariants."""
        raise AttributeError("Value objects can not have new attributes")

    def __delattr__(self, name):
        """Not allowed removing of invariants."""
        raise AttributeError("Not allowed to delete any invariant" " in value objects")

    @property
    def invariants(self):
        """Value objects are defined by its invariants."""
        return copy.deepcopy(self.__dict__)

    def __eq__(self, other):
        """Equality is defined comparing invariants."""
        return (
            self.__class__.__qualname__ == other.__class__.__qualname__
            and self.__dict__ == other.__dict__
        )


class ValueBuilder:
    """Builder of value objects."""

    _default = {"key": "value"}

    def __init__(self):
        """Build a value object for testing purposes."""
        self._invariants = self._default

    def with_invariants(self, **kwargs):
        """Set invariants of value object."""
        self._invariants = kwargs
        return self

    def build(self):
        """Return the constructed object."""
        return Value(**self._invariants)


class EntityId(Value):
    """Entity identification value class."""

    def __init__(self, uuid):
        """Id value example for testing purposes."""
        super().__init__(uuid=uuid)


class EntityIdBuilder:
    """EntityId for testing purpose."""

    def __init__(self):
        """Build entity id."""
        self._uuid = uuid.uuid4()

    def with_uuid(self, hex_value):
        """Fix uuid from its hex value."""
        self._uuid = uuid.UUID(hex_value)

    def build(self):
        """Build EntityId object."""
        return EntityId(self._uuid)


class Entity:
    """Example of entity for testing purpose."""

    class Created(Value):
        """Entity is created."""

        def apply_on(self, entity):
            """Apply on entity."""
            attributes = self.invariants
            entity.id = EntityId(attributes.pop("uuid"))
            for key, value in attributes.items():
                setattr(entity, key, value)

    class Modified(Value):
        """Entity is modified."""

        def apply_on(self, entity):
            """Apply on entity."""
            attributes = self.invariants
            for key, value in attributes.items():
                setattr(entity, key, value)

    def __init__(self):
        """Have states inside."""
        self.id = None

    def create(self, uuid, int_attr):
        """Create entity with a new id and some attributes."""
        self.trigger_event(self.Created(uuid=uuid, int_attr=int_attr))

    def modify(self, int_attr, string_attr):
        """Modify some attributes of entity."""
        self.trigger_event(self.Modified(int_attr=int_attr, string_atrr=string_attr))

    def trigger_event(self, event):
        """Trigger event to modify inner attributes."""
        event.apply_on(self)


class EntityBuilder:
    """Poor builder for a empty entity."""

    def build(self):
        """Return empty entity object."""
        return Entity()


class EventDtoBuilder:
    """Builder for creating event data transfer objects."""

    _DEFAULT = {
        "entity_name": "tests.builders:Entity",
        "entity_uuid": uuid.uuid4(),
        "event_name": "Created",
        "created_on": datetime.datetime.now(),
        "version": 0,
        "event_pars": {
            "a_uuid": uuid.UUID(int=1),
            "a_datetime": datetime.datetime.now().isoformat(),
            "an_int": random.randint(1, 1000),
            "a_float": 2.0,
            "a_string": "string",
        },
    }

    _id = 1

    def __init__(self):
        """
        Builder of event dtos for testing purposes.

        Default value is a Created event_dto of Entity object.
        """
        self._event_dto = copy.deepcopy(self._DEFAULT)

    def by_entity(self, uuid, entity_name=None):
        """Set entity parameters."""
        self._event_dto["entity_uuid"] = uuid

        if entity_name:
            self._event_dto["entity_name"] = entity_name
        return self

    def named(self, name):
        """Set event name."""
        self._event_dto["event_name"] = name
        return self

    def created_on(self, when):
        """Set client event creation date (in str isoformat)."""
        self._event_dto["created_on"] = when
        return self

    def with_event_pars(self, *args, **kwargs):
        """Set event invariants/parametres as dict or by key/value."""
        if args:
            self._event_dto["event_pars"] = args[0]
        for key, value in kwargs.items():
            self._event_dto["event_pars"][key] = value
        return self

    def with_version(self, value):
        """Set event version/order in entity."""
        self._event_dto["version"] = value
        return self

    def build(self):
        """Retrieve the customized event_dto."""
        self._event_dto["id"] = self._id
        EventDtoBuilder._id += 1
        return EventDataTransfer(**self._event_dto)


def given_a_list_of_entity_event_dtos(number_of_events=5):
    """Create a list of event dtos for the same entity."""
    given = Given()

    entity_args = (uuid.uuid4(), "eventence._builders:Entity")
    event_dtos = [
        given.a_event_dto.by_entity(*entity_args)
        .named("Created")
        .with_event_pars({"uuid": uuid.uuid4(), "int_attr": 1})
        .build()
    ]

    for version in range(1, number_of_events):
        event_dtos.append(
            given.a_event_dto.by_entity(*entity_args)
            .with_version(version)
            .with_event_pars(
                {"int_attr": random.randint(1, 1000), "string_attr": "string"}
            )
            .named("Modified")
            .build()
        )

    for dto in event_dtos:
        dto.id = None

    return event_dtos
