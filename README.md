# Persistence infrastructure using event sourcing architecture

Using CQRS (Comand Query Responsability Segregation) and Event Sourcing architecture, this library performs the persistence of objects within an application.

If you are developing with hexagonal architecture, this library is usefull for developing persistence of objects in your application.
If you are using domain driven design, this library is usefull for generating the repositories of aggregates and domain events.

Anyway the design of library is done for not be coupled with these concepts.
It's horrible to have in an ORM infrastru

# Conditions to use the library.

Event sourcing bases are that every change in a system shall be done triggering an event. 

I'm trying to avoid inheritance of this library in your code. A clean code should not depend on many external libraries, even more in business logic.

But if the developer wants to use the library the class design of the objects to persist should have the following rules:
1. The class shall be *event oriented*. It means that every change in the state of object shall be done triggering an event.
2. If a method changes internally the state of the object, an event shall be trigered
3. The object shall have a id attribute. 
3. Events shall be invariants.
4. Events shall have a `trigger` method whith the object as arguments.

# Using within Domain Driven Design

Persistence doesn't really matters to domain specialists. It's an application issue the problem to access to old entities from previous sessions or how to save during time. But it is necessary to assure that entities and domain events implement interfaces compatible with the previous section.

To get a good implementtation

# Using within hexagonal architecture

The hexagonal architecture fix three layers for an application software:
- Domain layer: All business logic, should be inside this layer. I don't understand the use of `repositories` in domain. I think the domain experts cares of persistence. 
- Application layer: From my opinion, persistence shall be used inside this layer. In general, it's a good aproach to use it directly o develop a infrastructure model with `eventence` implementing persistence flow. 
- Infrastructure layer: This library is intented to be inside this layer. Aplication layer, should use this library 
If something happens in the domain, there is a domain event behind.

## How to use the library

1. The object to persist shall be *event oriented*.
2. The library should be used on an user case method:

``` python
from eventence.lib import eventize, StoreFactory, Projector


class AplicationService:
    
    def __init__(self):
        self.store = StoreFactory().create_store('sqlite', ':onmen:')

    def user_case_with_new_object_to(self, ):
        obj = MyObject(id=1)
        obj = eventize(obj)
        
        obj.method_with_change_of_state()
        self._store(save)

        
        
```


## Closing

Many of my decisions on the eventsourcing implementations are taking from more advanced project [Eventsourcing in python](https://github.com/johnbywater/eventsourcing) by John Bywater. His code is more formal and better than mine, but the purpose is to use this library in a simple  way by my application projects.




### Data media

The way data are stored (on memory, local database, server database, file system, ...) is not important and it's an infrastructure issue. The `EventDataMedia` interface has the responsability of implementing the writing and reading of events. Its injection is necessary to accessFor each media  `ProjectionDataMedia`  and `EventDataMedia` are two interfaces to encapsulate on the library the implementation

### Event Dispatcher

Domain and application services are comunicated should be with an `EventDispatcher`. Behind this class there is an infrastructure to inject and is a `Broker`. There is an `OnmemBroker` implementation for the comunication inside the application, but if you need the comunication between application, the developer should implement other brokers (`MosquittoBroker`, `RabittBroker`, ...).

## Library structure

The library has two main modules to use:
- `context`: contains classes to be used for the application of Domain Driven Design. I don't like a lot but you should inherit these classes for getting the domain to work. Domain should not depens on any external library and I'm breaking this principle but I don't know other way to work now:
  + `Event`: Every Domain Event should inherit from this class
  + `Entity`: Every Entity should inherit from this class
  + `Aggregate`: This is class to make transactions in domain. All the commands to change domain should be done using aggregates. All the aggregates (and domain services) are the command layer in CQRS. 
- `persistence`: Contains a factory to construct the two main clases to use for persistence:
the main two clases to implemnent CQRS.  
  + `AggregateStore`:  It's the best way to I think it's better to have a Store model than a Repository pattern, The store pattern is simple, two methods:
    * `save`, to persist an aggregate
    * `load`, t o get an aggregate from persist media.
  + `queries`: queries persistence is managed by `Projector` and you access to queries by projections. Projections need a data persistence component (which is not necessary to be )
- `infrastucture`: It contains implementations of interfaces I used for my apps. It works but the developer is free to implement the best way.
  
