from unittest.mock import patch, MagicMock

from eventence.factories import Persistence, DbEngine


class FeatureStoreFactory:
    def ucase_creating_sqlite_store(self):
        persistence = Persistence(DbEngine("sqlite", {"filename": ":memory:"}))

        store = persistence.create_store()

        assert store.__class__.__name__ == "Store"

    def ucase_creating_onmen_store(self):
        persistence = Persistence(DbEngine("onmem"))

        store = persistence.create_store()

        assert store.__class__.__name__ == "Store"


class FeatureProjectionSyncronization:
    def ucase_projection_is_registered(self):
        projection = MagicMock()
        with patch("eventence.factories.prj") as prj:
            persistence = Persistence(DbEngine("onmem"))
            persistence.register_projection(projection)

            projector = prj.Projector.return_value

            projector.register_projection.assert_called_with(projection)
