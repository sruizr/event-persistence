from eventence.stores.onmem import OnMemEventDataAdapter
from eventence.core.commands import (
    EntityAlreadyExist,
    EntityNotFound,
    EntityVersionAlreadyExist,
)
from eventence._builders import given_a_list_of_entity_event_dtos
import pytest


class FeatureEventDataAdapter:
    def _given_a_event_data_adapter(self):
        return OnMemEventDataAdapter()

    def ucase_entity_event_dtos_are_persisted(self):
        data = self._given_a_event_data_adapter()
        event_dtos = given_a_list_of_entity_event_dtos()
        entity_uuid = event_dtos[0].entity_uuid

        data.add_event_dtos(event_dtos)
        dtos = data.get_entity_event_dtos(entity_uuid)

        expected = event_dtos
        for index, dto in enumerate(expected):
            dto.id = index + 1

        assert dtos == expected

    def ucase_get_all_event_dtos_from_event_type_filter(self):
        data = self._given_a_event_data_adapter()

        data.add_event_dtos(given_a_list_of_entity_event_dtos(4))
        data.add_event_dtos(given_a_list_of_entity_event_dtos(5))
        data.add_event_dtos(given_a_list_of_entity_event_dtos(6))

        event_dtos = data.collect_event_dtos(2, ["Entity.Created"])
        assert len(event_dtos) == 2

        event_dtos = data.collect_event_dtos(7, ["Entity.Modified"])
        assert len(event_dtos) == 6

    def ucase_entity_not_found_is_raised(self):
        data = self._given_a_event_data_adapter()

        dtos_1 = given_a_list_of_entity_event_dtos()
        data.add_event_dtos(dtos_1)
        dtos_2 = given_a_list_of_entity_event_dtos()

        try:
            data.get_entity_event_dtos(dtos_2[0].entity_uuid)
            pytest.fail("EntityNotFound should be raised")
        except EntityNotFound:
            pass

    def ucase_entity_can_not_be_persisted_twice(self):
        data = self._given_a_event_data_adapter()

        dtos = given_a_list_of_entity_event_dtos()
        data.add_event_dtos(dtos)

        try:
            data.add_event_dtos(dtos[:1])
            pytest.fail("EntityAlreadyExist should be raised")
        except EntityAlreadyExist:
            pass

    def ucase_event_dtos_with_same_version_can_not_be_persisted(self):
        data = self._given_a_event_data_adapter()

        dtos = given_a_list_of_entity_event_dtos()
        data.add_event_dtos(dtos)

        try:
            data.add_event_dtos(dtos[2:3])
            pytest.fail("EntityVersionAlreadyExist should be raised")
        except EntityVersionAlreadyExist:
            pass

    def ucase_event_dto_out_of_order_can_not_be_inserted(self):
        data = self._given_a_event_data_adapter()

        dtos = given_a_list_of_entity_event_dtos()
        try:
            data.add_event_dtos(dtos[2:3])
            pytest.fail("EntityNotFound should be raised")
        except EntityNotFound:
            pass
