import uuid
from eventence._builders import Entity, Given, Value
from eventence.ports import EventDataTransfer
import datetime


class FeatureEventDataTranfering:
    def _given_a_event_dto(self):
        a_uuid = uuid.uuid4()
        return EventDataTransfer(
            "eventence._builders:Entity",
            a_uuid,
            0,
            "Created",
            {"uuid": 1, "str_attr": "str", "value": Value(a=2)},
            datetime.datetime.now(),
        )

    def ucase_event_dto_constructed_by_domain(self):
        entity = Given().a_entity.build()
        dto = self._given_a_event_dto()

        uuid_ = uuid.uuid1()
        event = entity.Created(uuid=uuid_, str_attr="str", value=Value(int=2))
        event.apply_on(entity)

        dto = EventDataTransfer.create_from_domain(entity, 0, event)

        assert dto.id is None
        assert dto.entity_name == "eventence._builders:Entity"
        assert dto.entity_uuid == entity.id.uuid
        assert dto.version == 0
        assert dto.event_name == "Created"
        assert dto.event_pars == {
            "uuid": uuid_,
            "str_attr": "str",
            "value": Value(int=2),
        }
        assert dto.created_on.date() == datetime.datetime.now().date()

    def ucase_event_dto_build_event(self):
        dto = self._given_a_event_dto()

        created = dto.build_event()
        assert type(created)
        assert created.invariants == {"uuid": 1, "str_attr": "str", "value": Value(a=2)}

    def ucase_event_dto_find_entity_class(self):
        dto = self._given_a_event_dto()
        assert Entity == dto.entity_class

    def ucase_event_dto_can_duplicates(self):
        dto = self._given_a_event_dto()

        copied_dto = dto.copy()
        assert copied_dto == dto

        copied_dto.id = 99
        assert copied_dto != dto
